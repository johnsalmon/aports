# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=perl-image-exiftool
pkgver=11.93
pkgrel=0
pkgdesc="Perl module for editing exif metadata in files"
url="https://www.sno.phy.queensu.ca/~phil/exiftool"
arch="noarch"
license="Artistic-1.0-Perl GPL-1.0-or-later"
depends="perl"
makedepends="perl-dev"
subpackages="$pkgname-doc exiftool"
source="https://www.sno.phy.queensu.ca/~phil/exiftool/Image-ExifTool-$pkgver.tar.gz"

builddir="$srcdir"/Image-ExifTool-$pkgver
build() {
	PERL_MM_USE_DEFAULT=1 perl Makefile.PL INSTALLDIRS=vendor
	make
}

check() {
	make test
}

package() {
	make DESTDIR="$pkgdir" install
	# creates file collision among perl modules
	find "$pkgdir" -name perllocal.pod -delete
	find "$pkgdir" -name .packlist -delete
}

exiftool() {
	pkgdesc="Tool for editing exif metadata in files"
	depends="perl-image-exiftool"
	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

sha512sums="e8d9dd34710e3346788e10a3b50e4d2a4ba1aeef1969efeec1ddcb7507378b313554e00587cda9c71a325f7d4d7238c91f743c67051d9cef8a012798551dcc59  Image-ExifTool-11.93.tar.gz"
