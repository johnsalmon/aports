# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=librsvg
pkgver=2.48.2
pkgrel=0
pkgdesc="SAX-based renderer for SVG files into a GdkPixbuf"
url="https://wiki.gnome.org/Projects/LibRsvg"
arch="all !s390x !mips !mips64" # rust
license="LGPL-2.1-or-later"
options="!check" # Failing
subpackages="$pkgname-dev $pkgname-doc $pkgname-dbg $pkgname-lang"
makedepends="$depends_dev bzip2-dev cairo-dev glib-dev cargo rust vala
	gobject-introspection-dev gtk+3.0-dev libcroco-dev libgsf-dev"
source="https://download.gnome.org/sources/librsvg/${pkgver%.*}/librsvg-$pkgver.tar.xz"

# secfixes:
#   2.46.2-r0:
#     - CVE-2019-20446

build() {
	export RUSTFLAGS="$RUSTFLAGS -C debuginfo=2"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--libexecdir=/usr/lib/$pkgname \
		--disable-static \
		--enable-vala
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="70d293c47b7097624f72590a1d55d4bfb104c052ab8bd7a4a7158b1554fece06bdbf28e34d263a7894f72d145b3aedf683adc48f2c9f10b4cc46e7f33fd14d1a  librsvg-2.48.2.tar.xz"
